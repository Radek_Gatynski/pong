const canvas = document.getElementById("pongCanvas");
const ctx = canvas.getContext('2d');

//table
const tableWidth = 800;
const tableHeigth = 500;
const lineSectioTablenWidth = 4;
const lineSectionTableHeight = 6;
canvas.width = tableWidth;
canvas.height = tableHeigth;

//paddles
const paddleaHeight = 100;
const paddleWidht = 20;
const paddleLeftPosition = 10;
const paddleRightPosition = tableWidth - 30;
let beginLeftPaddleY = (tableHeigth / 2) - 50;
let beginRightPaddleY = (tableHeigth / 2) - 50;

//ball
const ballSize = 20;
let ballX = tableWidth / 2 - ballSize / 2;
let ballY = tableHeigth / 2 - ballSize / 2;
let ballSpeedX = 3;
let ballSpeedY = -2;
//points
let userPoints = 0;
let aiPoints = 0;

function table() {
    ctx.fillStyle = "black";
    ctx.fillRect(0, 0, tableWidth, tableHeigth);
    for (let i = 20; i < tableHeigth; i = i + 30) {
        ctx.fillStyle = "grey";
        ctx.fillRect((tableWidth / 2) - 2, i, lineSectioTablenWidth, lineSectionTableHeight);
    }
}
function paddleLeft() {
    ctx.fillStyle = "white";
    ctx.fillRect(paddleLeftPosition, beginLeftPaddleY, paddleWidht, paddleaHeight);
}

function paddleRight() {
    ctx.fillStyle = "red";
    ctx.fillRect(paddleRightPosition, beginRightPaddleY, paddleWidht, paddleaHeight);
}

function rightPoddlePosition() {
    let halfRightPaddlePosition = beginRightPaddleY + paddleaHeight / 2;
    let halfBallPosition = ballY + ballSize/2;
    if (ballX > 300 && ballX<700) {
        if(halfRightPaddlePosition - halfBallPosition > 150){
            beginRightPaddleY -= 10 + Math.floor((Math.random()*10)+1);
        }
        else if(halfRightPaddlePosition- halfBallPosition > 50){
            beginRightPaddleY -= 5 + Math.floor((Math.random()*10)+1);
        }
        else if(halfRightPaddlePosition - halfBallPosition < -150){
            beginRightPaddleY += 10 + Math.floor((Math.random()*10)+1);
        }
        else if(halfRightPaddlePosition- halfBallPosition < -50){
            beginRightPaddleY += 5 + Math.floor((Math.random()*10)+1);
        }
    }
    else if ( ballX > 100 && ballX <= 300 ) {
        if(halfRightPaddlePosition - halfBallPosition >150){
            beginRightPaddleY -= 3 + Math.floor((Math.random()*2)+1);

        }
        else if(halfRightPaddlePosition - halfBallPosition < -150){
            beginRightPaddleY += 3 + Math.floor((Math.random()*2)+1);
        }
    }
    
    if(beginRightPaddleY <= -50) beginRightPaddleY = -50 + Math.floor((Math.random()*100)+1);
    if(beginRightPaddleY >= 550) beginRightPaddleY = 550 - Math.floor((Math.random()*100)+1);
}

topCanvas = canvas.offsetTop;
function beginingLeftPaddle(e) {
    beginLeftPaddleY = (e.clientY - topCanvas) - paddleaHeight / 2;
    if (beginLeftPaddleY > tableHeigth - paddleaHeight) {
        beginLeftPaddleY = tableHeigth - paddleaHeight
    }
    if (beginLeftPaddleY <= 0) {
        beginLeftPaddleY = 0;
    }
}

function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1) ) + min;
}
function ball() {
    ctx.fillStyle = "white";
    ctx.fillRect(ballX, ballY, ballSize, ballSize);
   
    ballX += ballSpeedX;
    ballY += ballSpeedY;
    if( (ballY+ballSize) >= tableHeigth){
        ballSpeedY =- ballSpeedY;
        acceleration();
    }
    if(ballY <= 0){
        ballSpeedY =- ballSpeedY;
        acceleration();
    }
    if( (ballX == paddleLeftPosition+paddleWidht) && (ballY+ballSize > beginLeftPaddleY) && (ballY < beginLeftPaddleY+paddleaHeight)){
        ballSpeedX =- ballSpeedX;
        acceleration();
    }
    if( (ballX+ballSize == paddleRightPosition) &&  (ballY+ballSize > beginRightPaddleY) && (ballY < beginRightPaddleY+paddleaHeight)){
        ballSpeedX =- ballSpeedX;
        acceleration();  
    }
    if(ballX<15){
        aiPoints = aiPoints+1;
        reoladBall();
    }
    if(ballX+ballSize > 785){
        userPoints = userPoints + 1;
        reoladBall();
    }

}
function acceleration() {
    if (ballSpeedX > 0 && ballSpeedX < 16) ballSpeedX += 1;
    else if (ballSpeedX < 0 && ballSpeedX > 16) ballSpeedX -= 1;
    else if (ballSpeedY > 0 && ballSpeedY < 16) ballSpeedY += 1;
    else ballSpeedY -= 0.5;
}
function reoladBall(){
    console.log(userPoints + " : " + aiPoints);
    ballX = tableWidth / 2 - ballSize / 2;
    ballY = tableHeigth / 2 - ballSize / 2;
    ballSpeedX = getRndInteger(-3,3);
    ballSpeedY = getRndInteger(-3,3);
    if( (ballSpeedX == 0)){
        reoladBall();
    };
}



document.getElementById("pongCanvas").addEventListener("mousemove", beginingLeftPaddle);

function game(){
    table();
    ball();
    paddleLeft();
    paddleRight();
    rightPoddlePosition();
   
    requestAnimationFrame(game);
}
requestAnimationFrame(game);
